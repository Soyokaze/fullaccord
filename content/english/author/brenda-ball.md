---
title: "Brenda Ball"
image: "images/team/brenda_ball.jpg"
email: ""
social:
---
Brenda Ball started going to Christian youth camp when she was 9 years old. She continued every year after that, first as a camper and then as a staff member for the next 15 years. She took a break of 7 years when her children were small but then returned as a counselor, dean of women and as a cook for the next 29 years. She has been cooking for Senior Week and All Age Week for the past 15+ years. She is a Speech Language Pathologist and currently works with students who are preschool aged through fifth grade in the public schools. She has two wonderful children and two precious grandsons.