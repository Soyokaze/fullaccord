---
title: "Camp Roles"
date: 2020-06-01
image: images/camproles/camp_entrance.jpg
author: Robbie Nichols
description : "Full Accord Camp Roles"
---
**Director** - The two directors of Full Accord are tasked with scheduling, assigning staff, and defining responsibilities for those who serve with Full Accord. The most important task is providing a safe and spiritually sound community for participants to grow spiritually, mentally, physically, and socially.

**Bible Teacher** - Bible teachers are tasked with teaching the message of God with reverence, soundness, and joy. Bible teachers will be given freedom to cultivate the topics provided to them in a way that best serves the needs of the students.

**Dean of Men/Women** - The Dean of Men and the Dean of Women are tasked with supporting the counselors and staff of their respective gender. Any problems for which counselors need support can be brought to their Dean. Any significant problem which a Dean may face is to be brought before the directors. Dean’s are responsible for scheduling sound and appropriate dorm devos.

**Counselor** - Counselors are tasked with being a leader, mentor, and cheerleader for the campers placed in their group. Counselors will be teamed up with another of the opposite gender so that both can address the unique needs of the campers in their group. Counselors are responsible for keeping track of points that their campers receive for various challenges. Any questions or problems for counselors are to be given to the Dean of Men or Dean of Women.

**Chaplain** - The Chaplain is tasked with creating spiritual activities and challenges for the campers, including the creation and hosting of the Bible Bowl challenge. The chaplain will also look for baptized male campers that can speak during a devo towards the end of the week of camp. Chaplains will be cheerleaders for the spiritual growth of all campers and staff.

**Activity Coordination Team** - The Activities team is tasked with both creating safe and enjoyable activities for the campers to join together in, as well as coordinating Activity Coaches and campers to meet with one another during scheduled times which they will communicate to the camp. The Activities team will work to create opportunities for the campers to grow spiritually, physically, mentally and socially. The activity coordinator will lean upon the camp Chaplain to create any spiritual growth activities.

**Activity Coach** - Activity coaches are teachers who will meet with campers online to teach them skills related to spiritual, physical, emotional, and social wellbeing. Activity Coaches will work with the Activity Coordinator to communicate what time they will meet with students, as well as informing campers in advance what they will be learning/doing, and what items they may need to achieve their task by publishing their activities on fullaccord.com.

**Event Coordination Team** - The Events team coordinate big events that can accommodate more participants and age ranges. We will schedule one event in the evening before the campfire devotional. Concerts, silly/messy presentations, those sorts of camp activity.

**Challenges Coordination Team** - The Challenges team designs and facilitates optional challenges that run for an entire day (8:00AM - 12:00AM). Participants can earn points for completing/participating in the challenge for the day. things like a scavenger hunt, make a video of a bible character, art contest, some quick competition in a bracket, etc. that can be submitted to counselors and put together for part of the morning announcements the following day.

**Parents** - Parents are a vital part of this camp, don't drop them off and forget about them - they still need your help this week! Please help you camper find activiteis and encourage attendance in all the spiritual aspects of camp.

**Camper** - Campers jump in, get involved, make new friends, and have lots of Christian fun.